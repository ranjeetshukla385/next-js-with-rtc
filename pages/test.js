
import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
var RTCMultiConnection = require('rtcmulticonnection');



const myTestComponent = () => {
    var conn = RTCMultiConnection;
    // this line is VERY_important
    conn.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    // if you want audio+video conferencing
    conn.session = {
        audio: true,
        video: true
    };

    conn.openOrJoin('3');
    return (<Container maxWidth="sm">
        <Box my={4}>
            <div>Hello  this is test page</div>
        </Box>
    </Container>);
}
export default myTestComponent;